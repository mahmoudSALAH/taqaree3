package com.Battalion.kartella;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	EditText Name;
	
	TextView user_name;
	TextView user_lat;
	TextView user_lng;
	
	Button sendBT;
	Button recieveBT;
	
	String name = "";
	String date = "";
	double lat ;
	double lng ;
	
	String responseText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		final double longitude;
		final double latitude;
		Name = (EditText) findViewById(R.id.name);
		sendBT = (Button) findViewById(R.id.send);
		recieveBT = (Button) findViewById(R.id.receive);
		 
		user_name = (TextView) findViewById(R.id.user_name);
		//user_lat = (TextView) findViewById(R.id.user_lat);
		//user_lng = (TextView) findViewById(R.id.user_lng);
		 
		 LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		    // Define the criteria how to select the locatioin provider -> use
		    // default
		    Criteria criteria = new Criteria();
		   String provider = locationManager.getBestProvider(criteria, false);
		    Location location = locationManager.getLastKnownLocation(provider);
		    
		    
		    
			 lat =  (location.getLatitude());
			  lng =  (location.getLongitude());
			  
		sendBT.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				name = Name.getText().toString();
				
				 
				 
				Calendar c = Calendar.getInstance();
		        System.out.println("Current time => "+c.getTime());

		        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		        final String formattedDate = df.format(c.getTime());
		        
		        
		        
		        final ProgressDialog mainDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Loading ...", true);
		        mainDialog.setCancelable(true);

		        new Thread(new Runnable() {
		            public void run() {
		            	try {
		            		System.out.println("http://www.battaliondigital.com/kartella/ws/ws.php"+"?I="+"true"+ "&&name="+name+"&&lat="+String.valueOf(lat)+"&&lng="+String.valueOf(lng)+"&&timedate="+formattedDate);
		            		  URL url = new URL("http://www.battaliondigital.com/kartella/ws/ws.php"+"?I="+"true"+ "&&name="+name+"&&lat="+String.valueOf(lat)+"&&lng="+String.valueOf(lng)+"&&timedate="+formattedDate);
		            		  HttpURLConnection con = (HttpURLConnection) url.openConnection();
		            		  readStream(con.getInputStream());
		            		  } catch (Exception e) {
		            		  e.printStackTrace();
		            		}

		            	
		            	//sendThisParametrs("true", name, lng, lat, formattedDate);
		            }
		          }).start();
		        
		        
		        
			}
		});
		
		
		
		recieveBT.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				name = Name.getText().toString();				 
				Calendar c = Calendar.getInstance();
		        System.out.println("Current time => "+c.getTime());
		        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		        
		        final String formattedDate = df.format(c.getTime());	 
		        final ProgressDialog mainDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Loading ...", true);
		        mainDialog.setCancelable(true);

		        new Thread(new Runnable() {
		            public void run() {
		            	try {
		            		  System.out.println("http://www.battaliondigital.com/kartella/ws/ws.php"+"?P="+"true"+ "&&id="+Name.getText().toString());
		            		  URL url = new URL("http://www.battaliondigital.com/kartella/ws/ws.php"+"?P="+"true"+ "&&id="+Name.getText().toString());
		            		  HttpURLConnection con = (HttpURLConnection) url
		            		    .openConnection();
		            		  readStream(con.getInputStream());
		            		  } catch (Exception e) {
		            		  e.printStackTrace();
		            		}

		            	
		            	//sendThisParametrs("true", name, lng, lat, formattedDate);
		            }
		          }).start();
     
			}
		});
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void sendThisParametrs(String bool, String name, double lng, double lat, String datetime)
	{
		
			HttpClient httpclient = new DefaultHttpClient();
			
			//http://www.battaliondigital.com/kartella/ws/ws.php?O=true&&name=mahmoud
		    HttpPost httppost = new HttpPost("http://www.battaliondigital.com/kartella/ws/ws.php" + "?f=" + bool+ "&&name="+name+"&&lat="+String.valueOf(lat)+"&&lng="+String.valueOf(lng)+"&&datetime="+datetime);
		   
		    
		    HttpResponse response = null;
			try {
				response = httpclient.execute(httppost);
				
			} 
			
			catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			 
			
			if (response != null)
			{
				responseText = GetResponseContent(response.getEntity());
				System.out.println("ffffffffffffffffffffffffffffffffffff" + responseText);
				
				
				JSONObject json = null;
				JSONArray arr;
				try {
					json = new JSONObject(responseText);
					arr = json.getJSONArray("users");
					//Will used in the large project
					/**
					for (int i = 0; i < arr.length(); i++)
					{
					
						//Will used in the large project
						//ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
						//postVars.add(new BasicNameValuePair("user_id", cmp.id));
						
						
					}		
					*/
					System.out.println("ffffffffffffffffffffffffffffffffffff" + arr.get(0));
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				//System.out.println(response.getEntity());
			}
			
			else 
			{
				//responseText =  "null";
			}
				//return GetResponseContent(response.getEntity());
			//return "error";
	}
	
	private void readStream(InputStream in) throws JSONException {
		  BufferedReader reader = null;
		  try {
		    reader = new BufferedReader(new InputStreamReader(in));
		    String line = "";
		    while ((line = reader.readLine()) != null) 
		    {
		    	
		    	//JSONObject json = new JSONObject(line);

		    	// get LL json object
		    	//JSONObject json_LL = json.getJSONObject("users");

		    	// get value from LL Json Object
		    	//String str_value=json_LL.getString("user_name"); //<< get value here
		    	JSONObject json = null;
				JSONArray arr;
				
					json = new JSONObject(line);
					arr = json.getJSONArray("users");
					//Will used in the large project
					/**
					for (int i = 0; i < arr.length(); i++)
					{
					
						//Will used in the large project
						//ArrayList<NameValuePair> postVars = new ArrayList<NameValuePair>();
						//postVars.add(new BasicNameValuePair("user_id", cmp.id));
						
						
					}		
					*/
					
					user user = new user(arr.getJSONObject(0));
					System.out.println("User Name Is" + user.user_name);
					//System.out.println("User Name Is" + user.user_lat);
					 //new DownloadFilesTask().execute(); 
					//user_name.setText(user.user_name.toString());
					//user_lng.setText(user.user_lng.toString());
					//user_lat.setText(user.user_lat);
					/** Here We Retrieve The Information **/
					
					user_name.setText("id: "+user.user_id+",name: "+user.user_name+",lat: "+user.user_lat+",lng: "+user.user_lng);
					System.out.println(line);
		    }
		  } catch (IOException e) {
		    e.printStackTrace();
		  } finally 
		  {
		    
			  if (reader != null) {
		      try {
			        reader.close();
			      } 
		      catch (IOException e) {
		        e.printStackTrace();
		        }
		    }
		  }
		  
		  
	}
	
	
	private static String GetResponseContent(HttpEntity entity)
	{
		StringBuilder sb = new StringBuilder();
		try {
		    BufferedReader reader = 
		           new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
		    String line = null;

		    while ((line = reader.readLine()) != null) {
		        sb.append(line);
		    }
		}
		catch (IOException e) { e.printStackTrace(); }
		catch (Exception e) { e.printStackTrace(); }
		return sb.toString();

	}
	
	
	private class DownloadFilesTask extends AsyncTask<Void, Void, Void> {
      

        @Override
        protected Void doInBackground(Void... params) {
        	user_name.setText("da");
        	return null;
        }
        protected void onPostExecute(Void result) {
              
       }          
   } 
}
